﻿using UnityEngine;
using System.Collections;

public class PlayerActor : Actor
{
    // Fields
    public float playerWalkSpeed; // Determines player walk speed
    public float playerJumpInterval;
    private Vector3 playerJump;

    // Functions
    protected override void Start()
    {
        playerWalkSpeed = 0.018f;
        base.Start();
    }

    protected override void FixedUpdate()
    {
        HandleWalk();
        //Jump (playerJump);
        base.FixedUpdate();
    }

    public void Walk(float x, float y)
    {
        state.PlayerRequestWalk(x, y);
    }

    protected void HandleWalk()
    {
        // TODO: Fix diagonal zoom zoom movement
        if (state.playerDoWalk)
        {
            // Throw input values into a local Vector3, grab the magnitude after input, normalize main vector3 (sets magnitude to 1)
            Vector3 playerWalk = new Vector3(state.playerDoWalkX, state.playerDoWalkY, 0.0f);
            float magnitude = playerWalk.magnitude;
            playerWalk.Normalize();

            // playerWalk is added to WalkModifier to slow down the directions specified above, ex. if playerWalk(-1,0,0) then add (.5,0,0)
            // At this point magnitude works as an additional modifier on diagonals, we're using original magnitude of the input values
            playerWalk = playerWalk * playerWalkSpeed * magnitude;

            // Finally, move transform
            base.HandleWalk(playerWalk);
        }
    }

    protected override void AnimationCheck()
    {
        if (state.playerDoWalk)
        {
            if (state.playerDoWalkY > 0)
            {
                animator.SetTrigger("trigWalkUp");
            }
            else if (state.playerDoWalkY < 0)
            {
                animator.SetTrigger("trigWalkDown");
            }
            else if (state.playerDoWalkX > 0)
            {
                animator.SetTrigger("trigWalkRight");
            }
            else if (state.playerDoWalkX < 0)
            {
                animator.SetTrigger("trigWalkLeft");
            }
        }
        else // Set idle
        {
            // Trigger idle
            animator.SetTrigger("trigIdle");
        }
    }
    //protected void GetJumpInput()
    //{
    //    Vector3 translateValue = new Vector3(0,0,Input.GetKeyDown("space"));
    //    translateValue = translateValue * playerJumpInterval;
    //}
}