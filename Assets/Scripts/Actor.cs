﻿using UnityEngine;
using System.Collections;
/* ----------------------------------------------------------------------------------
 * ● Actor refers to the entities that make up our Scene; EnemyActor, PlayerActor,
 * PropActor, etc.
 * ---------------------------------------------------------------------------------- */
public class Actor : MonoBehaviour
{
    // Member variables & Class initialization
    protected ActorState state;
    protected Transform transform;
    protected Animator animator;

    // Functions
    protected virtual void Start()
    {
        state = new ActorState(); // Controls all state for actors
        transform = GetComponent<Transform>();
        animator = GetComponent<Animator>();
    }

    protected virtual void FixedUpdate()
    {
        AnimationCheck();
        ClampActorToBoundary();
        state.ResetDoStates();
    }

    protected void ClampActorToBoundary()
    {
        Vector3 actorClampValue = new Vector3(Mathf.Clamp(transform.position.x, -1.44f, 1.44f ),
                                                Mathf.Clamp(transform.position.y, -0.886f, 0.699f),
                                                Mathf.Clamp(transform.position.z, 0.0f, 0.0f));

        transform.position = actorClampValue;
    }

    protected virtual void HandleWalk(Vector3 actorWalk)
    {
        transform.Translate(actorWalk);
    }

    protected virtual void AnimationCheck()
    {
    }

    //protected virtual void Jump(Vector3 moveValues)
    //{
    //    transform.Translate(moveValues);
    //}
}
