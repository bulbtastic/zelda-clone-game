﻿using UnityEngine;
using System.Collections;
/* ----------------------------------------------------------------------------------
 * ● This class exists to accept input from the player. Since we're polling for 
 * input, everything sits in Update(). When input is received, we go back to the Actor,
 * who then usually checks state to see if an action is valid.
 * ---------------------------------------------------------------------------------- */

public class PlayerController : MonoBehaviour 
{
    private PlayerActor playerActor;

    // Use this for initialization
    private void Start () 
    {
        playerActor = GetComponent<PlayerActor>();
    }
    
    // Update is called once per frame, and we want to grab input on a per-frame basis
    private void Update () 
    {
        if (Input.GetAxisRaw("Horizontal") != 0.0f || Input.GetAxisRaw("Vertical") != 0.0f)
        {
            playerActor.Walk(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        }
    }
}
