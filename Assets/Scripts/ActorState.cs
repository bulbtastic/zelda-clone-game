﻿using UnityEngine;
using System.Collections;

public class ActorState
{
    // Member variables
    public bool playerDoWalk;
    public float playerDoWalkX;
    public float playerDoWalkY;
    public bool playerDoJump;


    public void PlayerRequestWalk(float x, float y)
    {
        if (!playerDoJump) // Conditions for when we can walk will go here
        {
            playerDoWalk = true;
            playerDoWalkX = x;
            playerDoWalkY = y;
        }
    }

    public void ResetDoStates()
    {
        playerDoWalk = false;
        playerDoJump = false;
    }
}
